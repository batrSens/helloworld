// +build dev

package gen

import "net/http"

// Assets contains project assets.
var Assets http.FileSystem = http.Dir("./gen/proj")
