package main

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"path"
)

func exer(err error) {
	if err != nil {
		panic("ERROR: " + err.Error())
	}
}

func main() {
	fmt.Println("hello world1")

	//assets := gen.Assets
	//
	//readFile(assets, "", "mew", 0)
}

func readFile(fileSystem http.FileSystem, source, dest string, level int) {
	file, err := fileSystem.Open(source)
	exer(err)

	defer file.Close()

	stat, err := file.Stat()
	exer(err)

	if stat.IsDir() {
		fmt.Println(ntime("    ", level), "|->", stat.Name())

		err = os.Mkdir(dest, os.ModePerm)
		exer(err)

		infoes, err := file.Readdir(0)
		exer(err)

		for _, info := range infoes {
			readFile(fileSystem, path.Join(source, info.Name()), path.Join(dest, info.Name()), level + 1)
		}
	} else {
		fmt.Println(ntime("    ", level), "|||", stat.Name())
		newFile, err := os.Create(dest)
		exer(err)

		buff := bytes.Buffer{}

		_, err = buff.ReadFrom(file)
		exer(err)

		_, err = newFile.ReadFrom(bytes.NewBuffer(bytes.ReplaceAll(buff.Bytes(), []byte("$KEK$"), []byte("mynewproject"))))
		exer(err)

		err = newFile.Close()
		exer(err)
	}
}

func ntime(s string, n int) string {
	res := ""

	for i := 0; i < n; i++ {
		res += s
	}

	return res
}
